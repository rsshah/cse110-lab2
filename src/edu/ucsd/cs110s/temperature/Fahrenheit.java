package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}

	public String toString() {
		return getValue() + " F";
	}

	@Override
	public Temperature toCelsius() {
		float cVal = (getValue() - 32) / 1.8f;
		
		return new Celsius(cVal);
	}

	@Override
	public Temperature toFahrenheit() {
		return this;
	}
}
