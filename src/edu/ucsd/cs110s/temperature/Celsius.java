package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}

	public String toString() {
		return getValue() + " C";
	}

	@Override
	public Temperature toCelsius() {
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		float fVal = getValue() * 1.8f + 32;
		return new Celsius(fVal);
	}
}
